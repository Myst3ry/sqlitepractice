package com.myst3ry.sqlitepractice.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myst3ry.sqlitepractice.OnNoteClickListener;
import com.myst3ry.sqlitepractice.R;
import com.myst3ry.sqlitepractice.data.local.model.Note;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.NoteHolder> {

    private List<Note> mNotes;
    private OnNoteClickListener mListener;

    public NotesAdapter(final OnNoteClickListener listener) {
        this.mNotes = new ArrayList<>();
        this.mListener = listener;
    }

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NoteHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder holder, int position) {
        final Note note = getNote(position);

        holder.mNoteDate.setText(note.getDate());
        holder.mNoteTitle.setText(note.getTitle());
        holder.mNoteBody.setText(note.getBody());
    }

    @Override
    public int getItemCount() {
        return mNotes != null ? mNotes.size() : 0;
    }

    public void setNotes(final List<Note> notes) {
        if (notes != null) {
            mNotes = notes;
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (!mNotes.isEmpty()) {
            mNotes.clear();
        }
    }

    private Note getNote(final int position) {
        return mNotes.get(position);
    }

    final class NoteHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.note_date)
        TextView mNoteDate;
        @BindView(R.id.note_title)
        TextView mNoteTitle;
        @BindView(R.id.note_body)
        TextView mNoteBody;

        @OnClick(R.id.note_container)
        public void onClick() {
            final Note note = getNote(getLayoutPosition());
            mListener.onNoteClick(note.getId());
        }

        NoteHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
