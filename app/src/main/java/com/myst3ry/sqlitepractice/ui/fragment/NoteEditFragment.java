package com.myst3ry.sqlitepractice.ui.fragment;

import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.myst3ry.sqlitepractice.DBWorkerThread;
import com.myst3ry.sqlitepractice.DateUtils;
import com.myst3ry.sqlitepractice.R;
import com.myst3ry.sqlitepractice.data.local.model.Note;
import com.myst3ry.sqlitepractice.ui.activity.NoteDetailActivity;

import java.util.Date;

import butterknife.BindView;

public final class NoteEditFragment extends BaseFragment {

    public static final String TAG = NoteEditFragment.class.getSimpleName();

    @BindView(R.id.note_edit_title)
    EditText mNoteEditTitle;
    @BindView(R.id.note_edit_body)
    EditText mNoteEditBody;
    @BindView(R.id.btn_save_edit_note)
    Button mButtonSave;

    private long mNoteId;

    private final DBWorkerThread mWorkerThread = DBWorkerThread.getInstance();

    public static NoteEditFragment newInstance(final long id, final String title, final String body) {
        final NoteEditFragment fragment = new NoteEditFragment();
        final Bundle args = new Bundle();
        args.putLong(ARG_ID, id);
        args.putString(ARG_TITLE, title);
        args.putString(ARG_BODY, body);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_note_edit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListeners();
        initUI();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        final MenuItem item = menu.findItem(R.id.action_edit);
        item.setIcon(getResources().getDrawable(R.drawable.ic_close_white_24dp, null));
    }

    private void setListeners() {
        mButtonSave.setOnClickListener(v -> {
            final String editedDate = DateUtils.parseDate(new Date());
            final String editedTitle = mNoteEditTitle.getText().toString();
            final String editedBody = mNoteEditBody.getText().toString();

            if (!editedTitle.isEmpty() && !editedBody.isEmpty()) {
                final Note note = new Note(mNoteId, editedDate, editedTitle, editedBody);
                updateNote(note);
                completeUpdating(note);
            } else {
                showToast(getString(R.string.empty_fields_err));
            }
        });
    }

    private void initUI() {
        if (getArguments() != null) {
            final String title = getArguments().getString(ARG_TITLE);
            final String body = getArguments().getString(ARG_BODY);
            mNoteId = getArguments().getLong(ARG_ID);

            mNoteEditTitle.setText(title);
            mNoteEditBody.setText(body);
        }
    }

    private void updateNote(final Note note) {
        final Message msg = new Message();
        msg.obj = note;
        msg.what = DBWorkerThread.MSG_UPDATE_NOTE;
        mWorkerThread.startTask(msg);
    }

    private void completeUpdating(final Note note) {
        if (mContext instanceof NoteDetailActivity) {
            ((NoteDetailActivity) mContext).onUpdate(note);
        }
    }
}
