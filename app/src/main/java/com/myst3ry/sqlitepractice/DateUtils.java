package com.myst3ry.sqlitepractice;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class DateUtils {

    private static final String OUTPUT_DATE_FORMAT = "dd.MM.yyyy HH:mm:ss";

    public static String parseDate(final Date date) {
        try {
            final SimpleDateFormat outputFormat = new SimpleDateFormat(OUTPUT_DATE_FORMAT, Locale.getDefault());
            return outputFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return date.toString();
        }
    }

    private DateUtils() { }
}