package com.myst3ry.sqlitepractice.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import com.myst3ry.sqlitepractice.BuildConfig;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {

    protected static final String ARG_ID = BuildConfig.APPLICATION_ID + "arg.ID";
    protected static final String ARG_DATE = BuildConfig.APPLICATION_ID + "arg.DATE";
    protected static final String ARG_TITLE = BuildConfig.APPLICATION_ID + "arg.TITLE";
    protected static final String ARG_BODY = BuildConfig.APPLICATION_ID + "arg.BODY";

    protected Context mContext;
    private Unbinder mUnbinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUnbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    protected void showToast(final String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
