package com.myst3ry.sqlitepractice.ui.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myst3ry.sqlitepractice.R;
import com.myst3ry.sqlitepractice.data.local.NoteStyleStorage;

import butterknife.BindView;

public final class NoteShowFragment extends BaseFragment {

    public static final String TAG = NoteShowFragment.class.getSimpleName();

    @BindView(R.id.note_title)
    TextView mNoteTitle;
    @BindView(R.id.note_body)
    TextView mNoteBody;
    @BindView(R.id.note_date)
    TextView mNoteDate;

    private NoteStyleStorage mNoteStyleStorage;

    public static NoteShowFragment newInstance(final String date, final String title, final String body) {
        NoteShowFragment fragment = new NoteShowFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DATE, date);
        args.putString(ARG_TITLE, title);
        args.putString(ARG_BODY, body);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNoteStyleStorage = new NoteStyleStorage(mContext);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_note_show, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mNoteStyleStorage != null) {
            setNoteStyle();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        final MenuItem item = menu.findItem(R.id.action_edit);
        item.setIcon(getResources().getDrawable(R.drawable.ic_note_edit_open_white_24dp, null));
    }

    private void initUI() {
        if (getArguments() != null) {
            final String noteDate = getArguments().getString(ARG_DATE);
            final String noteTitle = getArguments().getString(ARG_TITLE);
            final String noteBody = getArguments().getString(ARG_BODY);

            mNoteDate.setText(noteDate);
            mNoteTitle.setText(noteTitle);
            mNoteBody.setText(noteBody);
        }
    }

    private void setNoteStyle() {
        final int titleColor = mNoteStyleStorage.getTitleTextColor();
        final int titleSize = mNoteStyleStorage.getTitleTextSize();
        final int titleTypeface = mNoteStyleStorage.getTitleTextTypeface();

        final int bodyColor = mNoteStyleStorage.getBodyTextColor();
        final int bodySize = mNoteStyleStorage.getBodyTextSize();
        final int bodyTypeface = mNoteStyleStorage.getBodyTextTypeface();

        setDateVisibility(mNoteStyleStorage.getDateVisibility());
        setNoteTitleStyle(titleColor, titleSize, titleTypeface);
        setNoteBodyStyle(bodyColor, bodySize, bodyTypeface);
    }

    private void setDateVisibility(final boolean isDateVisible) {
        mNoteDate.setVisibility(isDateVisible ? View.VISIBLE : View.GONE);
    }

    private void setNoteTitleStyle(final int color, final int size, final int typeface) {
        mNoteTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        mNoteTitle.setTypeface(mNoteTitle.getTypeface(), typeface);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mNoteTitle.setTextColor(mContext.getResources().getColor(color, null));
        } else {
            mNoteTitle.setTextColor(mContext.getResources().getColor(color));
        }
    }

    private void setNoteBodyStyle(final int color, final int size, final int typeface) {
        mNoteBody.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        mNoteBody.setTypeface(mNoteBody.getTypeface(), typeface);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mNoteBody.setTextColor(mContext.getResources().getColor(color, null));
        } else {
            mNoteBody.setTextColor(mContext.getResources().getColor(color));
        }
    }
}
