package com.myst3ry.sqlitepractice.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.myst3ry.sqlitepractice.BuildConfig;
import com.myst3ry.sqlitepractice.DBWorkerThread;
import com.myst3ry.sqlitepractice.R;
import com.myst3ry.sqlitepractice.data.local.model.Note;
import com.myst3ry.sqlitepractice.ui.fragment.NoteEditFragment;
import com.myst3ry.sqlitepractice.ui.fragment.NoteShowFragment;

public final class NoteDetailActivity extends BaseActivity {

    private static final String EXTRA_NOTE_ID = BuildConfig.APPLICATION_ID + "extra.NOTE_ID";

    private Note mNote;
    private boolean isEditMode;

    private final DBWorkerThread mWorkerThread = DBWorkerThread.getInstance();

    public static Intent newIntent(final Context context) {
        return new Intent(context, NoteDetailActivity.class);
    }

    public static Intent newOpenIntent(final Context context, @NonNull final Long noteId) {
        final Intent intent = newIntent(context);
        intent.putExtra(EXTRA_NOTE_ID, noteId);
        return intent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.note_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        switch (id) {
            case R.id.action_edit:
                switchContent(mNote, isEditMode);
                return true;
            case R.id.action_delete:
                deleteCurrentNoteFromDb(mNote.getId());
                return true;
            case R.id.action_style:
                openStyleSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onUpdate(final Note note) {
        showContent(note);
        mNote = note;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(R.string.note_detail_title);
        getNoteFromDb(getIntent().getLongExtra(EXTRA_NOTE_ID, 0L));
    }

    private void switchContent(final Note note, final boolean isEditMode) {
        if (!isEditMode) {
            showContent(note);
        } else {
            showEditableContent(note);
        }
    }

    private void showContent(final Note note) {
        hideKeyboard();
        replaceFragment(NoteShowFragment.newInstance(note.getDate(), note.getTitle(), note.getBody()), NoteShowFragment.TAG);
        isEditMode = true;
    }

    private void showEditableContent(final Note note) {
        replaceFragment(NoteEditFragment.newInstance(note.getId(), note.getTitle(), note.getBody()), NoteEditFragment.TAG);
        isEditMode = false;
    }

    private void replaceFragment(final Fragment fragment, final String tag) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_container, fragment, tag)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void openStyleSettings() {
        startActivity(NoteStyleActivity.newIntent(this));
    }

    private void getNoteFromDb(final long noteId) {
        final Message msg = new Message();
        msg.obj = noteId;
        msg.what = DBWorkerThread.MSG_GET_NOTE;
        msg.replyTo = new Messenger(new NoteDetailHandler());
        mWorkerThread.startTask(msg);
    }

    private void deleteCurrentNoteFromDb(final long noteId) {
        final Message msg = new Message();
        msg.obj = noteId;
        msg.what = DBWorkerThread.MSG_DELETE_NOTE;
        mWorkerThread.startTask(msg);
        finish();
    }

    private final class NoteDetailHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == DBWorkerThread.MSG_GET_NOTE) {
                if (msg.obj instanceof Note) {
                    final Note note = (Note) msg.obj;
                    showContent(note);
                    mNote = note;
                }
            }
        }
    }
}
