package com.myst3ry.sqlitepractice.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.RadioGroup;
import android.widget.Switch;

import com.myst3ry.sqlitepractice.R;
import com.myst3ry.sqlitepractice.StyleConstants;
import com.myst3ry.sqlitepractice.data.local.NoteStyleStorage;

import java.util.Objects;

import butterknife.BindView;

public final class NoteStyleActivity extends BaseActivity {

    @BindView(R.id.switch_show_date)
    Switch mDateSwitch;

    @BindView(R.id.radio_group_title_size)
    RadioGroup mTitleSizeGroup;
    @BindView(R.id.radio_group_title_color)
    RadioGroup mTitleColorGroup;
    @BindView(R.id.radio_group_title_typeface)
    RadioGroup mTitleTypefaceGroup;

    @BindView(R.id.radio_group_body_size)
    RadioGroup mBodySizeGroup;
    @BindView(R.id.radio_group_body_color)
    RadioGroup mBodyColorGroup;
    @BindView(R.id.radio_group_body_typeface)
    RadioGroup mBodyTypefaceGroup;

    private NoteStyleStorage mNoteStyleStorage;

    private boolean dateSwitchState;
    private int titleColor, titleSize, titleTypeface;
    private int bodyColor, bodySize, bodyTypeface;

    public static Intent newIntent(final Context context) {
        return new Intent(context, NoteStyleActivity.class);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        applyStyleSettings();
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_style);
        mNoteStyleStorage = new NoteStyleStorage(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        setDateSwitchListener();
        setTitleRadioListeners();
        setBodyRadioListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(R.string.note_style_title);
        syncCheckedState();
    }

    private void setDateSwitchListener() {
        mDateSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> dateSwitchState = isChecked);
    }

    private void setTitleRadioListeners() {
        mTitleColorGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.title_color_black:
                    titleColor = StyleConstants.COLOR_BLACK;
                    break;
                case R.id.title_color_green:
                    titleColor = StyleConstants.COLOR_DARK_GREEN;
                    break;
                case R.id.title_color_red:
                    titleColor = StyleConstants.COLOR_DARK_RED;
                    break;
                default:
                    break;
            }
        });

        mTitleSizeGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.title_size_small:
                    titleSize = StyleConstants.SIZE_SMALL;
                    break;
                case R.id.title_size_medium:
                    titleSize = StyleConstants.SIZE_MEDIUM;
                    break;
                case R.id.title_size_large:
                    titleSize = StyleConstants.SIZE_LARGE;
                    break;
                default:
                    break;
            }
        });

        mTitleTypefaceGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.title_typeface_normal:
                    titleTypeface = Typeface.NORMAL;
                    break;
                case R.id.title_typeface_bold:
                    titleTypeface = Typeface.BOLD;
                    break;
                case R.id.title_typeface_italic:
                    titleTypeface = Typeface.ITALIC;
                    break;
                default:
                    break;
            }
        });
    }

    private void setBodyRadioListeners() {
        mBodyColorGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.body_color_black:
                    bodyColor = StyleConstants.COLOR_BLACK;
                    break;
                case R.id.body_color_green:
                    bodyColor = StyleConstants.COLOR_DARK_GREEN;
                    break;
                case R.id.body_color_red:
                    bodyColor = StyleConstants.COLOR_DARK_RED;
                    break;
                default:
                    break;
            }
        });

        mBodySizeGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.body_size_small:
                    bodySize = StyleConstants.SIZE_SMALL;
                    break;
                case R.id.body_size_medium:
                    bodySize = StyleConstants.SIZE_MEDIUM;
                    break;
                case R.id.body_size_large:
                    bodySize = StyleConstants.SIZE_LARGE;
                    break;
                default:
                    break;
            }
        });

        mBodyTypefaceGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.body_typeface_normal:
                    bodyTypeface = Typeface.NORMAL;
                    break;
                case R.id.body_typeface_bold:
                    bodyTypeface = Typeface.BOLD;
                    break;
                case R.id.body_typeface_italic:
                    bodyTypeface = Typeface.ITALIC;
                    break;
                default:
                    break;
            }
        });
    }

    private void applyStyleSettings() {
        mNoteStyleStorage.saveDateVisibility(dateSwitchState);
        saveTitleTextStyle();
        saveBodyTextStyle();
    }

    private void saveTitleTextStyle() {
        if (mNoteStyleStorage != null) {
            mNoteStyleStorage.saveTitleTextColor(titleColor);
            mNoteStyleStorage.saveTitleTextSize(titleSize);
            mNoteStyleStorage.saveTitleTextTypeface(titleTypeface);
        }
    }

    private void saveBodyTextStyle() {
        if (mNoteStyleStorage != null) {
            mNoteStyleStorage.saveBodyTextColor(bodyColor);
            mNoteStyleStorage.saveBodyTextSize(bodySize);
            mNoteStyleStorage.saveBodyTextTypeface(bodyTypeface);
        }
    }

    private void syncCheckedState() {
        if (mNoteStyleStorage != null) {
            mDateSwitch.setChecked(mNoteStyleStorage.getDateVisibility());

            final int tColor = mNoteStyleStorage.getTitleTextColor();
            final int tSize = mNoteStyleStorage.getTitleTextSize();
            final int tTypeface = mNoteStyleStorage.getTitleTextTypeface();

            final int bColor = mNoteStyleStorage.getBodyTextColor();
            final int bSize = mNoteStyleStorage.getBodyTextSize();
            final int bTypeface = mNoteStyleStorage.getBodyTextTypeface();

            syncTitleColorRadioChecked(tColor);
            syncTitleSizeRadioChecked(tSize);
            syncTitleTypefaceRadioChecked(tTypeface);

            syncBodyColorRadioChecked(bColor);
            syncBodySizeRadioChecked(bSize);
            syncBodyTypefaceRadioChecked(bTypeface);
        }
    }

    private void syncTitleColorRadioChecked(final int color) {
        int id = 0;
        switch (color) {
            case StyleConstants.COLOR_BLACK:
                id = R.id.title_color_black;
                break;
            case StyleConstants.COLOR_DARK_GREEN:
                id = R.id.title_color_green;
                break;
            case StyleConstants.COLOR_DARK_RED:
                id = R.id.title_color_red;
                break;
            default:
                break;
        }
        mTitleColorGroup.check(id);
    }

    private void syncTitleSizeRadioChecked(final int size) {
        int id = 0;
        switch (size) {
            case StyleConstants.SIZE_SMALL:
                id = R.id.title_size_small;
                break;
            case StyleConstants.SIZE_MEDIUM:
                id = R.id.title_size_medium;
                break;
            case StyleConstants.SIZE_LARGE:
                id = R.id.title_size_large;
                break;
            default:
                break;
        }
        mTitleSizeGroup.check(id);
    }

    private void syncTitleTypefaceRadioChecked(final int typeface) {
        int id = 0;
        switch (typeface) {
            case StyleConstants.TYPEFACE_NORMAL:
                id = R.id.title_typeface_normal;
                break;
            case StyleConstants.TYPEFACE_BOLD:
                id = R.id.title_typeface_bold;
                break;
            case StyleConstants.TYPEFACE_ITALIC:
                id = R.id.title_typeface_italic;
                break;
            default:
                break;
        }
        mTitleTypefaceGroup.check(id);
    }

    private void syncBodyColorRadioChecked(final int color) {
        int id = 0;
        switch (color) {
            case StyleConstants.COLOR_BLACK:
                id = R.id.body_color_black;
                break;
            case StyleConstants.COLOR_DARK_GREEN:
                id = R.id.body_color_green;
                break;
            case StyleConstants.COLOR_DARK_RED:
                id = R.id.body_color_red;
                break;
            default:
                break;
        }
        mBodyColorGroup.check(id);
    }

    private void syncBodySizeRadioChecked(final int size) {
        int id = 0;
        switch (size) {
            case StyleConstants.SIZE_SMALL:
                id = R.id.body_size_small;
                break;
            case StyleConstants.SIZE_MEDIUM:
                id = R.id.body_size_medium;
                break;
            case StyleConstants.SIZE_LARGE:
                id = R.id.body_size_large;
                break;
            default:
                break;
        }
        mBodySizeGroup.check(id);
    }

    private void syncBodyTypefaceRadioChecked(final int typeface) {
        int id = 0;
        switch (typeface) {
            case StyleConstants.TYPEFACE_NORMAL:
                id = R.id.body_typeface_normal;
                break;
            case StyleConstants.TYPEFACE_BOLD:
                id = R.id.body_typeface_bold;
                break;
            case StyleConstants.TYPEFACE_ITALIC:
                id = R.id.body_typeface_italic;
                break;
            default:
                break;
        }
        mBodyTypefaceGroup.check(id);
    }

}
