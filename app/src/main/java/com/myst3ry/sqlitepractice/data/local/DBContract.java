package com.myst3ry.sqlitepractice.data.local;

public final class DBContract {

    /* TABLE NOTE */
    public static final String DB_TABLE_NOTE = "note";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_BODY = "body";

    private DBContract() {}
}
