package com.myst3ry.sqlitepractice;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;

import com.myst3ry.sqlitepractice.data.local.DBManager;
import com.myst3ry.sqlitepractice.data.local.model.Note;

import java.util.ArrayList;
import java.util.List;

public final class DBWorkerThread extends HandlerThread {

    public static final int MSG_GET_ALL_NOTES = 101;
    public static final int MSG_GET_NOTE = 102;
    public static final int MSG_ADD_NOTE = 103;
    public static final int MSG_UPDATE_NOTE = 104;
    public static final int MSG_DELETE_NOTE = 105;

    private static final String WORKER_NAME = "db_worker";

    private final WorkerHandler mHandler;
    private final DBManager mDBManager;

    private static volatile DBWorkerThread INSTANCE;

    public static DBWorkerThread getInstance() {
        DBWorkerThread instance = INSTANCE;
        if (instance == null) {
            synchronized (DBWorkerThread.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new DBWorkerThread(WORKER_NAME);
                }
            }
        }
        return instance;
    }

    public void startTask(final Message msg) {
        if (mHandler != null) {
            mHandler.sendMessage(msg);
        }
    }

    private DBWorkerThread(final String name) {
        super(name);
        this.start();
        mHandler = new WorkerHandler(this.getLooper());
        mDBManager = DBManager.getInstance(null);
    }

    private void getAllNotes(final Message msg) {
        final List<Note> notes = new ArrayList<>(mDBManager.getAllNotes());
        final Message replyMessage = new Message();
        replyMessage.obj = notes;
        replyMessage.what = MSG_GET_ALL_NOTES;

        try {
            msg.replyTo.send(replyMessage);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void getNote(final Message msg) {
       final Note note = mDBManager.getNote(msg.obj instanceof Long ? (Long) msg.obj : 0L);
       final Message replyMessage = new Message();
       replyMessage.obj = note;
       replyMessage.what = MSG_GET_NOTE;

        try {
            msg.replyTo.send(replyMessage);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void addNote(final Message msg) {
        mDBManager.addNote(msg.obj instanceof Note ? (Note) msg.obj : null);
    }

    private void updateNote(final Message msg) {
        mDBManager.updateNote(msg.obj instanceof Note ? (Note) msg.obj : null);
    }

    private void deleteNote(final Message msg) {
        mDBManager.deleteNote(msg.obj instanceof Long ? (Long) msg.obj : 0L);
    }


    private final class WorkerHandler extends Handler {

        WorkerHandler(final Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_GET_ALL_NOTES:
                    getAllNotes(msg);
                    break;
                case MSG_GET_NOTE:
                    getNote(msg);
                    break;
                case MSG_ADD_NOTE:
                    addNote(msg);
                    break;
                case MSG_UPDATE_NOTE:
                    updateNote(msg);
                    break;
                case MSG_DELETE_NOTE:
                    deleteNote(msg);
                    break;
                default:
                    break;
            }
        }
    }
}
