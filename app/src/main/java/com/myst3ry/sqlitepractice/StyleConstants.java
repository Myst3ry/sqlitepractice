package com.myst3ry.sqlitepractice;

import android.graphics.Typeface;

public final class StyleConstants {

    public static final int COLOR_BLACK = android.R.color.black;
    public static final int COLOR_DARK_GREEN = android.R.color.holo_green_dark;
    public static final int COLOR_DARK_RED = android.R.color.holo_red_dark;

    public static final int SIZE_SMALL = 14;
    public static final int SIZE_MEDIUM = 18;
    public static final int SIZE_LARGE = 22;

    public static final int TYPEFACE_NORMAL = Typeface.NORMAL;
    public static final int TYPEFACE_BOLD = Typeface.BOLD;
    public static final int TYPEFACE_ITALIC = Typeface.ITALIC;

    private StyleConstants() { }
}
