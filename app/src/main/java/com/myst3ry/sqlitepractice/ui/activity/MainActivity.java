package com.myst3ry.sqlitepractice.ui.activity;

import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.myst3ry.sqlitepractice.OnNoteClickListener;
import com.myst3ry.sqlitepractice.R;
import com.myst3ry.sqlitepractice.DBWorkerThread;
import com.myst3ry.sqlitepractice.data.local.model.Note;
import com.myst3ry.sqlitepractice.ui.adapter.NotesAdapter;

import java.util.List;

import butterknife.BindView;

public final class MainActivity extends BaseActivity {

    @BindView(R.id.notes_rec_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.fab_add)
    FloatingActionButton mFloatingButtonAdd;

    private NotesAdapter mAdapter;
    private List<Note> mNotes;

    private DBWorkerThread mWorkerThread = DBWorkerThread.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initAdapter();
        initRecyclerView();
        initFAB();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(R.string.main_title);

        getNotesFromDb();
        showNotes(mNotes);
    }

    private void initAdapter() {
        mAdapter = new NotesAdapter(new OnNoteClickListenerImpl());
    }

    private void initRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initFAB() {
        mFloatingButtonAdd.setOnClickListener(v -> startActivity(NoteCreateActivity.newIntent(this)));
    }

    private void showNotes(final List<Note> notes) {
        if (mAdapter != null && notes != null) {
            mAdapter.clear();
            mAdapter.setNotes(notes);
        }
    }

    private void getNotesFromDb() {
        final Message msg = new Message();
        msg.what = DBWorkerThread.MSG_GET_ALL_NOTES;
        msg.replyTo = new Messenger(new MainHandler());
        mWorkerThread.startTask(msg);
    }

    private final class OnNoteClickListenerImpl implements OnNoteClickListener {
        @Override
        public void onNoteClick(@NonNull final Long id) {
            startActivity(NoteDetailActivity.newOpenIntent(MainActivity.this, id));
        }
    }

    private final class MainHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == DBWorkerThread.MSG_GET_ALL_NOTES) {
                final List<Note> notes = (List<Note>) msg.obj;
                mNotes = notes;
                showNotes(notes);
            }
        }
    }
}
