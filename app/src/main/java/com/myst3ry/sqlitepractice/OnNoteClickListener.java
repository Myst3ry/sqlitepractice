package com.myst3ry.sqlitepractice;

import android.support.annotation.NonNull;

public interface OnNoteClickListener {

    void onNoteClick(@NonNull final Long id);
}
