package com.myst3ry.sqlitepractice.data.local.model;

import android.os.Parcel;
import android.os.Parcelable;

public final class Note implements Parcelable {

    private long mId;
    private String mDate;
    private String mTitle;
    private String mBody;

    public Note(final long id, final String date, final String title, final String body) {
        this.mId = id;
        this.mDate = date;
        this.mTitle = title;
        this.mBody = body;
    }

    public Note(final String date, final String title, final String body) {
        this.mDate = date;
        this.mTitle = title;
        this.mBody = body;
    }

    protected Note(Parcel in) {
        mId = in.readLong();
        mDate = in.readString();
        mTitle = in.readString();
        mBody = in.readString();
    }

    public long getId() {
        return mId;
    }

    public void setId(final long id) {
        this.mId = id;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(final String date) {
        this.mDate = date;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(final String title) {
        this.mTitle = title;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(final String body) {
        this.mBody = body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mDate);
        dest.writeString(mTitle);
        dest.writeString(mBody);
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {

        @Override
        public Note createFromParcel(Parcel source) {
            return new Note(source);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };
}
