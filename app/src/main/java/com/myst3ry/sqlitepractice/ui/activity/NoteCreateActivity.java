package com.myst3ry.sqlitepractice.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.widget.Button;
import android.widget.EditText;

import com.myst3ry.sqlitepractice.DBWorkerThread;
import com.myst3ry.sqlitepractice.DateUtils;
import com.myst3ry.sqlitepractice.R;
import com.myst3ry.sqlitepractice.data.local.model.Note;

import java.util.Date;

import butterknife.BindView;

public final class NoteCreateActivity extends BaseActivity {

    @BindView(R.id.note_create_title)
    EditText mNoteCreateTitle;
    @BindView(R.id.note_create_body)
    EditText mNoteCreateBody;
    @BindView(R.id.btn_create_note)
    Button mButtonCreate;
    @BindView(R.id.btn_cancel_note)
    Button mButtonCancel;

    private final DBWorkerThread mWorkerThread = DBWorkerThread.getInstance();

    public static Intent newIntent(final Context context) {
        return new Intent(context, NoteCreateActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_create);
        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(R.string.note_create_title);
    }

    private void setListeners() {
        mButtonCreate.setOnClickListener(v -> {
            final String currentDate = DateUtils.parseDate(new Date());
            final String noteTitle = mNoteCreateTitle.getText().toString();
            final String noteBody = mNoteCreateBody.getText().toString();

            if (!noteTitle.isEmpty() && !noteBody.isEmpty()) {
                final Note newNote = new Note(currentDate, noteTitle, noteBody);
                createNote(newNote);
            } else {
                showToast(getString(R.string.empty_fields_err));
            }
        });

        mButtonCancel.setOnClickListener(v -> finish());
    }

    private void createNote(final Note note) {
        final Message msg = new Message();
        msg.obj = note;
        msg.what = DBWorkerThread.MSG_ADD_NOTE;
        mWorkerThread.startTask(msg);
        finish();
    }
}
