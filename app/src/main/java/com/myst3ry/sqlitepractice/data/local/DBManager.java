package com.myst3ry.sqlitepractice.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.myst3ry.sqlitepractice.data.local.model.Note;

import java.util.ArrayList;
import java.util.List;

public final class DBManager {

    private static final String TAG = "DBManager";
    private static final String ID_WHERE_CLAUSE = "id = ?";

    private final DBHelper mDBHelper;
    private SQLiteDatabase mDatabase = null;

    private static volatile DBManager INSTANCE;

    public static DBManager getInstance(final Context context) {
        DBManager instance = INSTANCE;
        if (instance == null) {
            synchronized (DBManager.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new DBManager(context);
                }
            }
        }
        return instance;
    }

    private DBManager(final Context context) {
        mDBHelper = new DBHelper(context);
    }

    public void addNote(final Note note) {
        try {
            mDatabase = mDBHelper.getWritableDatabase();
            final ContentValues values = getNoteContentValues(note);
            mDatabase.beginTransaction();

            addNoteInternal(values);

            mDatabase.setTransactionSuccessful();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
                mDatabase = null;
            }
        }
    }

    public Note getNote(final long noteId) {
        Note note = null;
        try {
            mDatabase = mDBHelper.getReadableDatabase();
            mDatabase.beginTransaction();

            final String id = String.valueOf(noteId);
            final Cursor cursor = mDatabase.query(DBContract.DB_TABLE_NOTE, null, ID_WHERE_CLAUSE,
                    new String[]{id}, null, null,null);
            note = parseNoteCursor(cursor);
            cursor.close();

            mDatabase.setTransactionSuccessful();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
                mDatabase = null;
            }
        }

        return note;
    }

    public List<Note> getAllNotes() {
        List<Note> notes = null;
        try {
            mDatabase = mDBHelper.getReadableDatabase();
            mDatabase.beginTransaction();

            final Cursor cursor = mDatabase.query(DBContract.DB_TABLE_NOTE, null, null,
                    null, null, null, DBContract.COLUMN_ID + " DESC");
            notes = parseNoteListCursor(cursor);
            cursor.close();

            mDatabase.setTransactionSuccessful();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
                mDatabase = null;
            }
        }
        return notes;
    }

    public void updateNote(final Note note) {
        try {
            mDatabase = mDBHelper.getWritableDatabase();
            final ContentValues values = getNoteContentValues(note);
            mDatabase.beginTransaction();

            updateNoteInternal(note.getId(), values);

            mDatabase.setTransactionSuccessful();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
                mDatabase = null;
            }
        }
    }

    public void deleteNote(final long noteId) {
        try {
            mDatabase = mDBHelper.getWritableDatabase();
            mDatabase.beginTransaction();

            deleteNoteInternal(noteId);

            mDatabase.setTransactionSuccessful();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (mDatabase != null) {
                if (mDatabase.inTransaction()) {
                    mDatabase.endTransaction();
                }
                mDatabase.close();
                mDatabase = null;
            }
        }
    }

    private ContentValues getNoteContentValues(final Note note) {
        final String noteDate = note.getDate();
        final String noteTitle = note.getTitle();
        final String noteBody = note.getBody();
        final ContentValues noteValues = new ContentValues();

        noteValues.put(DBContract.COLUMN_DATE, noteDate);
        noteValues.put(DBContract.COLUMN_TITLE, noteTitle);
        noteValues.put(DBContract.COLUMN_BODY, noteBody);

        return noteValues;
    }

    private void addNoteInternal(final ContentValues values) {
        if (mDatabase != null) {
            mDatabase.insert(DBContract.DB_TABLE_NOTE, null, values);
        }
    }

    private void updateNoteInternal(final long noteId, final ContentValues values) {
        if (mDatabase != null) {
            final String id = String.valueOf(noteId);
            mDatabase.update(DBContract.DB_TABLE_NOTE, values, ID_WHERE_CLAUSE, new String[]{id});
        }
    }

    private void deleteNoteInternal(final long noteId) {
        if (mDatabase != null) {
            final String id = String.valueOf(noteId);
            mDatabase.delete(DBContract.DB_TABLE_NOTE, ID_WHERE_CLAUSE, new String[]{id});
        }
    }

    private Note parseNoteCursor(final Cursor cursor) {
        Note note = null;
        if (cursor.moveToFirst()) {
            note = getNoteFromCursor(cursor);
        }
        return note;
    }

    private List<Note> parseNoteListCursor(final Cursor cursor) {
        List<Note> notes = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                notes.add(getNoteFromCursor(cursor));
                cursor.moveToNext();
            }
        }
        return notes;
    }

    private Note getNoteFromCursor(final Cursor cursor) {
        final long id = cursor.getLong(cursor.getColumnIndex(DBContract.COLUMN_ID));
        final String date = cursor.getString(cursor.getColumnIndex(DBContract.COLUMN_DATE));
        final String title = cursor.getString(cursor.getColumnIndex(DBContract.COLUMN_TITLE));
        final String body = cursor.getString(cursor.getColumnIndex(DBContract.COLUMN_BODY));
        return new Note(id, date, title, body);
    }
}
