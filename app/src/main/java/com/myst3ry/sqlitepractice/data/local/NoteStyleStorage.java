package com.myst3ry.sqlitepractice.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.myst3ry.sqlitepractice.StyleConstants;

public final class NoteStyleStorage {

    private static final String PREF_NOTE_STYLE_NAME = "note_style";

    private final Context mContext;

    public NoteStyleStorage(final Context context) {
        this.mContext = context;
    }

    /* Setters */

    public void saveDateVisibility(final boolean isDateShown) {
        getNoteStylePrefs().edit().putBoolean(SPContract.NOTE_DATE_KEY, isDateShown).apply();
    }

    public void saveTitleTextColor(final int titleTextColor) {
        getNoteStylePrefs().edit().putInt(SPContract.NOTE_TITLE_COLOR_KEY, titleTextColor).apply();
    }

    public void saveTitleTextSize(final int titleTextSize) {
        getNoteStylePrefs().edit().putInt(SPContract.NOTE_TITLE_SIZE_KEY, titleTextSize).apply();
    }

    public void saveTitleTextTypeface(final int titleTextTypeface) {
        getNoteStylePrefs().edit().putInt(SPContract.NOTE_TITLE_TYPEFACE_KEY, titleTextTypeface).apply();
    }

    public void saveBodyTextColor(final int bodyTextColor) {
        getNoteStylePrefs().edit().putInt(SPContract.NOTE_BODY_COLOR_KEY, bodyTextColor).apply();
    }

    public void saveBodyTextSize(final int bodyTextSize) {
        getNoteStylePrefs().edit().putInt(SPContract.NOTE_BODY_SIZE_KEY, bodyTextSize).apply();
    }

    public void saveBodyTextTypeface(final int bodyTextTypeface) {
        getNoteStylePrefs().edit().putInt(SPContract.NOTE_BODY_TYPEFACE_KEY, bodyTextTypeface).apply();
    }

    /* Getters */

    public boolean getDateVisibility() {
        return getNoteStylePrefs().getBoolean(SPContract.NOTE_DATE_KEY, true);
    }

    public int getTitleTextColor() {
        return getNoteStylePrefs().getInt(SPContract.NOTE_TITLE_COLOR_KEY, StyleConstants.COLOR_BLACK);
    }

    public int getTitleTextSize() {
        return getNoteStylePrefs().getInt(SPContract.NOTE_TITLE_SIZE_KEY, StyleConstants.SIZE_LARGE);
    }

    public int getTitleTextTypeface() {
        return getNoteStylePrefs().getInt(SPContract.NOTE_TITLE_TYPEFACE_KEY, StyleConstants.TYPEFACE_BOLD);
    }

    public int getBodyTextColor() {
        return getNoteStylePrefs().getInt(SPContract.NOTE_BODY_COLOR_KEY, StyleConstants.COLOR_BLACK);
    }

    public int getBodyTextSize() {
        return getNoteStylePrefs().getInt(SPContract.NOTE_BODY_SIZE_KEY, StyleConstants.SIZE_MEDIUM);
    }

    public int getBodyTextTypeface() {
        return getNoteStylePrefs().getInt(SPContract.NOTE_BODY_TYPEFACE_KEY, StyleConstants.TYPEFACE_NORMAL);
    }


    private SharedPreferences getNoteStylePrefs() {
        return mContext.getSharedPreferences(PREF_NOTE_STYLE_NAME, Context.MODE_PRIVATE);
    }
}
