package com.myst3ry.sqlitepractice.data.local;

public final class SPContract {

    public static final String NOTE_DATE_KEY = "note_date";

    public static final String NOTE_TITLE_COLOR_KEY = "note_title_color";
    public static final String NOTE_TITLE_SIZE_KEY = "note_title_size";
    public static final String NOTE_TITLE_TYPEFACE_KEY = "note_title_typeface";

    public static final String NOTE_BODY_COLOR_KEY = "note_body_color";
    public static final String NOTE_BODY_SIZE_KEY = "note_body_size";
    public static final String NOTE_BODY_TYPEFACE_KEY = "note_body_typeface";

    private SPContract() { }
}
