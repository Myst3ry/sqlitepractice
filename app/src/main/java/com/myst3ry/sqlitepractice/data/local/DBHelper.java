package com.myst3ry.sqlitepractice.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.myst3ry.sqlitepractice.data.local.DBContract.COLUMN_BODY;
import static com.myst3ry.sqlitepractice.data.local.DBContract.COLUMN_DATE;
import static com.myst3ry.sqlitepractice.data.local.DBContract.COLUMN_ID;
import static com.myst3ry.sqlitepractice.data.local.DBContract.COLUMN_TITLE;
import static com.myst3ry.sqlitepractice.data.local.DBContract.DB_TABLE_NOTE;

public final class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "notepad_db";
    private static final int DB_VERSION = 1;

    public DBHelper(final Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        deleteTables(db);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    private void createTables(final SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DB_TABLE_NOTE + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_DATE + " TEXT, " + COLUMN_TITLE + " TEXT, " + COLUMN_BODY + " TEXT)");
    }

    private void deleteTables(final SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_NOTE);
    }
}
