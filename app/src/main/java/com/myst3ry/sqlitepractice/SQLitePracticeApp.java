package com.myst3ry.sqlitepractice;

import android.app.Application;

import com.myst3ry.sqlitepractice.data.local.DBManager;

public final class SQLitePracticeApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initDB();
    }

    private void initDB() {
        DBManager.getInstance(this);
    }
}
